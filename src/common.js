const mapValueToPriority = {
    1: 'Low',
    2: 'Medium',
    3: 'High'
}

const storage = new StateStorage();

function getAllTasks() {
    let data = storage.get();
     return data ? data.tasksToDo.concat(data.tasksPending, data.tasksDone) : [];
}

function alertValid() {
    'use strict'

    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
        })
}